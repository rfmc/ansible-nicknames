famous_people:
    - short: newton
      full: Isaac Newton
      desc: English mathematician, physicist, astronomer, theologian, and author.
      years: 1642-1726

    - short: darwin
      full: Charles Darwin
      desc: English naturalist, geologist and biologist, best known for his contributions to the science of evolution
      years: 1809-1882

    - short: faraday
      full: Michael Faraday
      desc: British scientist who contributed to the study of electromagnetism and electrochemistry.
      years: 1791-1867

    - short: rutherford
      full: Ernest Rutherford
      desc: New Zealand-born British physicist who came to be known as the father of nuclear physics.
      years: 1871-1937

    - short: ramanujan
      full: Srinivasa Ramanujan
      desc: Indian mathematician who lived during the British Rule in India
      years: 1887-1920

    - short: koch
      full: Robert Koch
      desc: German physician and microbiologist.
      years: 1843-1910

    - short: churchill
      full: Winston Churchill
      desc: British politician, statesman, army officer, and writer, who was Prime Minister of the United Kingdom from 1940 to 1945 and again from 1951 to 1955
      years: 1874-1965

    - short: chandrasekhar
      full: Subrahmanyan Chandrasekhar
      desc: Indian American astrophysicist. His mathematical treatment of stellar evolution yielded many of the best current theoretical models of the later evolutionary stages of massive stars and black holes.
      years: 1910-1995

    - short: hodgkin
      full: Dorothy Hodgkin
      desc: British chemist who developed protein crystallography, for which she won the Nobel Prize in Chemistry.
      years: 1910-1994

    - short: crick
      full: Francis Harry Compton Crick
      desc: British molecular biologist, biophysicist, and neuroscientist, most noted for being a co-discoverer of the structure of the DNA molecule.
      years: 1916-2004

    - short: watson
      full: James Watson
      desc: American molecular biologist, geneticist and zoologist, best known as one of the co-discoverers of the structure of DNA.
      years: 1928-

    - short: franklin
      full: Rosalin Elsie Franklin
      desc: English chemist and X-ray crystallographer who made contributions to the understanding of the molecular structures of DNA, RNA, viruses, coal, and graphite.

    - short: hawking
      full: Stephen Hawking
      desc: English theoretical physicist, cosmologist, and author.
      years: 1942-2018

    - short: aristotle
      full: Aristotle
      desc: Ancient Greek philosopher and scientist. Along with Plato, he is considered the "Father of Western Philosophy"
      years: 384-332 BC

    - short: linnaeus
      full: Carl Linnaeus
      desc: Swedish botanist, physician, and zoologist who formalised binomial nomenclature, the modern system of naming organisms.
      years: 1707-1778

    - short: mendel
      full: Gregor Mendel
      desc: Scientist, creator of the science of genetics
      years: 1822-1884

    - short: leonardo
      full: Leonardo da Vinci
      desc: Italian polymath of the Renaissance whose areas of interest included invention, painting, sculpting, architecture, science, music, mathematics, engineering, literature, anatomy, geology, astronomy, botany, writing, history, and cartography.
      years: 1452-1519

    - short: democritus
      full: Democritus
      desc: Ancient Greek pre-Socratic philosopher primarily remembered today for his formulation of an atomic theory of the universe.
      years: 460-370 BC

    - short: dalton
      full: John Dalton
      desc: English chemist, physicist, and meteorologist. He is best known for introducing the atomic theory into chemistry, and for his research into colour blindness.
      years: 1766-1844

    - short: lewis
      full: Gilbert Lewis
      desc: American physical chemist known for the discovery of the covalent bond and his concept of electron pairs
      years: 1875-1946

    - short: mendeleev
      full: Dmitri Mendeleev
      desc: Russian chemist and inventor.
      years: 1834-1907

    - short: arrhenius
      full: Svarte Arrhenius
      desc: Swedish scientist. Known for his theory of ionic dissociation.
      years: 1859-1927

    - short: ostwald
      full: Friedrish Ostwald
      desc: German chemist. He received the Nobel Prize in Chemistry in 1909 for his work on catalysis, chemical equilibria and reaction velocities.
      years: 1853-1932

    - short: helmholtz
      full: Hermann von Helmholtz
      desc: German physician and physicist who made significant contributions in several scientific fields.
      years: 1921-1984

    - short: eratosthenes
      full: Eratosthenes
      desc: Greek mathematician, geographer, poet, astronomer, and music theorist.
      years: 276-194 BC

    - short: hippocrates
      full: Hippocrates
      desc: Greek physician of the Age of Pericles (Classical Greece), who is considered one of the most outstanding figures in the history of medicine.
      years: 460-370 BC

    - short: nightingate
      full: Florence Nightingale
      desc: English social reformer and statistician, and the founder of modern nursing.
      years: 1820-1910

    - short: lavoisier
      full: Antoine Lavoisier
      desc: French nobleman and chemist who was central to the 18th-century chemical revolution and who had a large influence on both the history of chemistry and the history of biology.
      years: 1743-1794

    - short: freud
      full: Sigmund Freud
      desc: Austrian neurologist and the founder of psychoanalysis.
      years: 1856-1939

    - short: fermi
      full: Enrico Fermi
      desc: Italian and naturalized-American physicist and the creator of the world's first nuclear reactor.
      years: 1901-1954

    - short: oppenheimer
      full: J. Robert Oppenheimer
      desc: American theoretical physicist; credited with being the "father of the atomic bomb" for their role in the Manhattan Project.
      years: 1904-1967

    - short: groves
      full: Leslie Groves
      desc: United States Army Corps of Engineers officer who oversaw the construction of the Pentagon and directed the Manhattan Project.
      years: 1896-1970

    - short: teller
      full: Edward Teller
      desc: Hungarian-American theoretical physicist who is known colloquially as "the father of the hydrogen bomb"
      years: 1908-2003

    - short: hubble
      full: Edwin Hubble
      desc: American astronomer. He played a crucial role in establishing the fields of extragalactic astronomy and observational cosmology.
      years: 1889-1953

    - short: ampere
      full: André-Marie Ampère
      desc: French physicist and mathematician who was one of the founders of the science of classical electromagnetism.
      years: 1775-1836

    - short: copernicus
      full: Nicolaus Copernicus
      desc: Renaissance-era mathematician and astronomer who formulated a model of the universe that placed the Sun rather than the Earth at the center of the universe.
      years: 1473-1543

    - short: curie
      full: Marie Curie
      desc: Polish and naturalized-French physicist and chemist who conducted pioneering research on radioactivity.
      years: 1867-1934

    - short: galileo
      full: Galileo Galilei
      desc: Italian astronomer, physicist and engineer, sometimes described as a polymath.
      years: 1564-1642

    - short: planck
      full: Max Planck
      desc: German theoretical physicist whose discovery of energy quanta won him the Nobel Prize in Physics.
      years: 1858-1947

    - short: carnot
      full: Sadi Carnot
      desc: French military engineer and physicist, often described as the "father of thermodynamics".
      years: 1796-1832

    - short: cauchy
      full: Augustin-Louis Cauchy
      desc: French mathematician, engineer and physicist.
      years: 1789-1857

    - short: weierstrass
      full: Karl Weierstrass
      desc: German mathematician often cited as the "father of modern analysis".
      years: 1815-1897

    - short: descartes
      full: René Descartes
      desc: French philosopher, mathematician, and scientist.
      years: 1596-1650

    - short: fermat
      full: Pierre de Fermat
      desc: French lawyer and mathematician.
      years: 1607-1665

    - short: leibniz
      full: Gottfried Wilhelm Leibniz
      desc: German polymath and philosopher in the history of mathematics and the history of philosophy.
      years: 1646-1716

    - short: mandelbrot
      full: Benoit Mandelbrot
      desc: Polish-born, French and American mathematician and polymath.
      years: 1924-2010

    - short: euclid
      full: Euclid
      desc: Greek mathematician, often referred to as the "founder of geometry"
      years: 4th-3rd century BC

    - short: euler
      full: Leonhard Euler
      desc: Swiss mathematician, physicist, astronomer, logician and engineer
      years: 1707-1783

    - short: laplace
      full: Pierre-Simon Laplace
      desc: French scholar whose work was important to the development of engineering, mathematics, statistics, physics and astronomy.
      years: 1749-1827

    - short: pascal
      full: Blaise Pascal
      desc: French mathematician, physicist, inventor, writer and Catholic theologian. 
      years: 1623-1662

    - short: heaviside
      full: Oliver Heaviside
      desc: English self-taught electrical engineer, mathematician, and physicist.
      years: 1850-1925

    - short: lorenz
      full: Edward Norton Lorenz
      desc: American mathematician, meteorologist, and a pioneer of chaos theory.
      years: 1917-2008

    - short: smith
      full: Adam Smith
      desc: Scottish economist, philosopher and author as well as a moral philosopher.
      years: 1723-1790

    - short: bernoulli
      full: Daniel Bernoulli
      desc: Swiss mathematician and physicist and was one of the many prominent mathematicians in the Bernoulli family.
      years: 1700-1782

    - short: friedman
      full: Milton Friedman
      desc: American economist who received the 1976 Nobel Memorial Prize in Economic Sciences for his research on consumption analysis, monetary history and theory and the complexity of stabilization policy.
      years: 1912-2006

    - short: siemens
      full: Werner von Siemens
      desc: German inventor and industrialist.
      years: 1816-1892

    - short: stephenson
      full: George Stephenson
      desc: English civil engineer and mechanical engineer.
      years: 1781-1848

    - short: westinghouse
      full: George Westinghouse
      desc: American entrepreneur and engineer who invented the railway air brake and was a pioneer of the electrical industry
      years: 1846-1914

    - short: zeppelin
      full: Ferdinand von Zeppelin
      desc: German general and later inventor of the Zeppelin rigid airships.
      years: 1838-1917

    - short: cessna
      full: Clyde Cessna
      desc: American aircraft designer, aviator, and founder of the Cessna Aircraft Corporation.
      years: 1879-1954

    - short: boeing
      full: William Boeing
      desc: American aviation pioneer who founded The Boeing Company.
      years: 1881-1956

    - short: nyquist
      full: Harry Nyquist
      desc: Swedish-born American electronic engineer who made important contributions to communication theory.

    - short: russell
      full: Bertrand Russell
      desc: British philosopher, logician, mathematician, historian, writer, social critic, political activist, and Nobel laureate.
      years: 1872-1970


cs_famous_people:
    - short: backus
      full: John Backus
      desc: American computer scientist. He directed the team that invented and implemented FORTRAN.
      years: 1924-2007

    - short: berners-lee
      full: Tim Berners-Lee
      desc: English engineer and computer scientist, best known as the inventor of the World Wide Web.
      years: 1955-

    - short: boole
      full: George Boole
      desc: English mathematician, philosopher and logician; he worked in the fields of differential equations and algebraic logic.
      years: 1815-1864

    - short: kahn
      full: Bob Kahn
      desc: American electrical engineer, who, along with Vint Cerf, invented the Transmission Control Protocol.
      years: 1938-

    - short: cerf
      full: Vint Cerf
      desc: American Internet pioneer, who is recognized as one of "the fathers of the Internet", sharing this title with TCP/IP co-inventor Bob Kahn.
      years: 1943-

    - short: chomsky
      full: Noam Chomsky
      desc: American linguist, philosopher, cognitive scientist, historian, political activist, and social critic.
      years: 1928-

    - short: aiken
      full: Howard H. Aiken
      desc: American physicist and a pioneer in computing
      years: 1900-1973

    - short: karpinski
      full: Jacek Karpinski
      desc: Polish pioneer in computer engineering and computer science.
      years: 1927-2010

    - short: karnaugh
      full: Maurice Karnaugh
      desc: American physicist and mathematician known for the Karnaugh map used in Boolean algebra.
      years: 1924-

    - short: knuth
      full: Donald Knuth
      desc: American computer scientist and mathematician. 
      years: 1938-

    - short: lamport
      full: Leslie Lamport
      desc: American computer scientist; best known for his seminal work in distributed systems and as the initial developer of the document preparation system LaTeX.
      years: 1941-

    - short: liskov
      full: Barbara Liskov
      desc: American computer scientist
      years: 1939-

    - short: lovelace
      full: Ada Lovelace
      desc: English mathematician and writer.
      years: 1815-1852

    - short: mccarthy
      full: John McCarthy
      desc: American computer scientist and cognitive scientist; one of the founders of the discipline of artificial intelligence.
      years: 1927-2011

    - short: mccluskey
      full: Edward J. McCluskey
      desc: Pioneer in the field of Electrical Engineering; known for the Quine-McCluskey algorithm.
      years: 1929-2016

    - short: naur
      full: Peter Naur
      desc: Danish computer science pioneer; his last name is the "N" in the BNF notation.
      years: 1928-2016

    - short: neumann
      full: John von Neumann
      desc: Hungarian-American mathematician, physicist, computer scientist, and polymath.
      years: 1903-1957

    - short: perlman
      full: Radia Perlman
      desc: American computer programmer and network engineer. She is most famous for her invention of the spanning-tree protocol.
      years: 1951-

    - short: ritchie
      full: Dennis Ritchie
      desc: American computer scientist. He created the C programming language and, with long-time colleague Ken Thompson, the Unix operating system.
      years: 1941-2011

    - short: shannon
      full: Claude Shannon
      desc: American mathematician, electrical engineer, and cryptographer known as "the father of information theory"
      years: 1916-2001

    - short: stallman
      full: Richard Stallman
      desc: American free software movement activist and programmer.
      years: 1953-

    - short: stroustrup
      full: Bjarne Stroustrup
      desc: Danish computer scientist, who is most notable for the creation and development of the C++ programming language.
      years: 1950-

    - short: hamming
      full: Richard Hamming
      desc: American mathematician whose work had many implications for computer engineering and telecommunications.
      years: 1915-1998

    - short: church
      full: Alonzo Church
      desc: American mathematician and logician; best known for the lambda calculus, Church–Turing thesis.
      years: 1903-1995

    - short: dijkstra
      full: Edsger W. Dijkstra
      desc: Dutch systems scientist, programmer, software engineer, science essayist, and early pioneer in computing science.
      years: 1930-2002

    - short: torvalds
      full: Linus Torvalds
      desc: Finnish–American software engineer who is the creator, and historically, the principal developer of the Linux kernel.
      years: 1969-

    - short: turing
      full: Alan Turing
      desc: English mathematician, computer scientist, logician, cryptanalyst, philosopher, and theoretical biologist.
      years: 1912-1954

    - short: zuse
      full: Konrad Zuse
      desc: German civil engineer, inventor and computer pioneer. His greatest achievement was the world's first programmable computer.
      years: 1910-1995
